from pathlib import Path
from functools import partial
import numpy as np
import pandas as pd
import os
from sktime.transformations.series.adapt import TabularToSeriesAdaptor
from sklearn.preprocessing import StandardScaler
from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor, TSFreshRelevantFeatureExtractor
from sktime.transformations.panel.rocket import Rocket, MiniRocket
from sktime.transformations.series.impute import Imputer
from sktime.datasets import load_from_ucr_tsv_to_dataframe
import glob
from sktime.classification.hybrid._hivecote_v2 import HIVECOTEV2
from sktime.classification.dictionary_based import WEASEL
import time
from joblib import Parallel, delayed
from sktime.transformations.panel.catch22 import Catch22
from sktime.classification.deep_learning.inceptiontime import InceptionTimeClassifier
from sktime.classification.deep_learning.resnet import ResNetClassifier
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv1D
from sktime.classification.deep_learning.cnn import CNNClassifier
from sktime.classification.deep_learning.macnn import MACNNClassifier
from abc import ABC, abstractmethod

def interpolate_scale_single(X):
    scaler = Imputer(method='linear') * TabularToSeriesAdaptor(StandardScaler())
    return scaler.fit_transform(X)
    
def interpolate_scale(X_train, X_test):
    return interpolate_scale_single(X_train), interpolate_scale_single(X_test)
    
def create_directory_if_not_exist(directory):
    Path(directory).mkdir(parents=True, exist_ok=True)
    
def read_dataset(dataset):
    X_train, y_train = load_from_ucr_tsv_to_dataframe(
        os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TRAIN.tsv")
    )
    X_test, y_test = load_from_ucr_tsv_to_dataframe(
        os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TEST.tsv")
    )
    return X_train, X_test, y_train, y_test


class Representation(ABC):
    def __init__(self, name, seed):
        self.name = name
        self.seed = seed
        self.model = None

class RCatch22(Representation):
    def __init__(self, seed):
        super().__init__('catch22', seed)
        self.model = Catch22(n_jobs=-1, replace_nans=True)

    def fit(self, X_train, y_train):
        pass

    def embeddings(self, X_test):
        return [(self.name, self.model.fit_transform(X_test).to_numpy())]

class RNeuralNet(Representation):
    def dict_param_format(self, d):
        return "__".join([f"{str(key).replace('_', '')}_{str(value).replace('_', '')}" for key, value in d.items()])

    def embeddings(self, X_test):
        layer_outputs = [l.output for l in self.model.model_.layers]
        intermediate_model = Model(inputs=self.model.model_.input, outputs=layer_outputs)
        input_data = self.model._check_convert_X_for_predict(X_test).transpose((0, 2, 1))
        layer_representations = intermediate_model.predict(input_data)
        other_meta = new_params = {'model': self.name, 'seed': self.seed}
        param_s = self.dict_param_format({**self.params,**other_meta})
        return [(f'{param_s}__{str(i.name)}'.lower(), x) for i, x in zip(layer_outputs, layer_representations)]

class RCNNClassifier(RNeuralNet):
    def __init__(self, seed, params=None):
        super().__init__('cnn', seed)
        self.params = params if params is not None else {}
        self.model = CNNClassifier(**self.params, random_state=seed)

    def fit(self, X_train, y_train):
        self.model.fit(X_train, y_train)


class RInceptionTimeClassifier(RNeuralNet):
    def __init__(self, seed, params=None):
        super().__init__('inception', seed)
        self.params = params if params is not None else {}
        self.model = InceptionTimeClassifier(**self.params, random_state=seed)

    def fit(self, X_train, y_train):
        self.model.fit(X_train, y_train)




#class RResNetClassifier(Representation):
#    def __init__(self, seed, params=None):
#        super().__init__('resnet', seed)
#        self.params = params if params is not None else {}
#        self.model = ResNetClassifier(**self.params, random_state=seed)
#
#    def fit(self, X_train, y_train):
#        self.model.fit(X_train, y_train)
#
#    def embeddings(self, X_test):
#        layer_outputs = [l.output for l in self.model.model_.layers]
#        intermediate_model = Model(inputs=self.model.model_.input, outputs=layer_outputs)
#        input_data = self.model._check_convert_X_for_predict(X_test).transpose((0, 2, 1))
#        layer_representations = intermediate_model.predict(input_data)
#        param_s = "__".join([f"{key}_{value}" for key, value in self.params.items()])
#        return [(f'{self.name}__{param_s}__{str(i.name)}'.lower(), x) for i, x in zip(layer_outputs, layer_representations)]


#def feature_factory(feature_type, seed):
#    from sktime.transformations.panel.catch22 import Catch22
#    feature_types = {
#        'tsfresh': TSFreshFeatureExtractor(default_fc_parameters="efficient", show_warnings=False, n_jobs=-1, disable_progressbar=True),
#        'rocket': Rocket(n_jobs=-1, random_state=seed),
#        'minirocket': MiniRocket(n_jobs=-1, random_state=seed),
#        'catch22': Catch22(n_jobs=-1, replace_nans=True)
#    }
#    return feature_types[feature_type]