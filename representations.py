from pathlib import Path
from functools import partial
import numpy as np
import pandas as pd
import os
from sktime.transformations.series.adapt import TabularToSeriesAdaptor
from sklearn.preprocessing import StandardScaler
from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor, TSFreshRelevantFeatureExtractor
from sktime.transformations.panel.rocket import Rocket, MiniRocket
from sktime.transformations.series.impute import Imputer
from sktime.datasets import load_from_ucr_tsv_to_dataframe
import glob
from sktime.classification.hybrid._hivecote_v2 import HIVECOTEV2
from sktime.classification.dictionary_based import WEASEL
import time
from joblib import Parallel, delayed
from sktime.transformations.panel.catch22 import Catch22
from sktime.classification.deep_learning.inceptiontime import InceptionTimeClassifier
from sktime.classification.deep_learning.tapnet import TapNetClassifier
from sktime.classification.deep_learning.resnet import ResNetClassifier
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv1D
from sktime.classification.deep_learning.cnn import CNNClassifier
from sktime.classification.deep_learning.macnn import MACNNClassifier
from abc import ABC, abstractmethod
import polars as pl
import argparse
from utils import *
from sktime.classification.deep_learning.resnet import ResNetClassifier


def parquet_format(model, seed, layer, n_layer, data, params=None):
    if params == None:
        params = {}
    return_format = []
    static_data = {
        'model': model,
        'seed': seed,
        'layer': layer,
        'n_layer': n_layer,
        'params_keys': [str(x) for x in list(params.keys())],
        'params_values': [str(x) for x in list(params.values())]
    }
    for i, row in list(enumerate(data)):
        return_format.append({
            **static_data,
            'n_example': i,
            'embedding': list(row),
        })
    return_df = pl.DataFrame(return_format)
    return return_df

class Representation(ABC):
    def __init__(self, name, seed, model):
        self.name = name
        self.seed = seed
        self.model = model

class RNeuralNet(Representation):
    def embeddings(self, X_test):
        layer_outputs = [l.output for l in self.model.model_.layers]
        intermediate_model = Model(inputs=self.model.model_.input, outputs=layer_outputs)
        input_data = self.model._check_convert_X_for_predict(X_test).transpose((0, 2, 1))
        layer_representations = intermediate_model.predict(input_data)
        return_list = []
        for i, (l, r) in enumerate(zip(layer_outputs, layer_representations)):
            layer_type = type(l._keras_history.layer).__name__
            df = parquet_format(model=type(self).__name__, seed=self.seed, layer=layer_type, n_layer=i, data=r.reshape(len(r), -1), params=self.params)
            return_list.append(df)
        return pl.concat(return_list)

    def fit(self, X_train, y_train):
        self.model.fit(X_train, y_train)

    def predict(self, X_test):
        return self.model.predict(X_test)

class RCNNClassifier(RNeuralNet):
    def __init__(self, seed, params=None):
        self.params = params if params is not None else {}
        super().__init__('cnn', seed, CNNClassifier(**self.params, random_state=seed))


class RInceptionTimeClassifier(RNeuralNet):
    def __init__(self, seed, params=None):
        self.params = params if params is not None else {}
        super().__init__('inception', seed, InceptionTimeClassifier(**self.params, random_state=seed))

class RTapNetClassifier(RNeuralNet):
    def __init__(self, seed, params=None):
        self.params = params if params is not None else {}
        super().__init__('tapnet', seed, TapNetClassifier(**self.params, random_state=seed))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--method', type=str, required=True)
    parser.add_argument('--seed', type=int, required=True)

    args = parser.parse_args()
    print(args)

    avail_models_list = [RCNNClassifier(seed=args.seed, params={'n_conv_layers': 3}), RInceptionTimeClassifier(seed=args.seed), RTapNetClassifier(seed=args.seed)]

    avail_models = {x.name: x for x in avail_models_list}

    model = avail_models[args.method.lower()]

    folder_path = 'embeddings'
    os.makedirs(folder_path, exist_ok=True)
    file_path = f'{folder_path}/model_{model.name}__dataset_{args.dataset}__seed_{model.seed}__set_test.parquet'

    if os.path.exists(file_path):
        exit(0)


    X_train, X_test, y_train, y_test = read_dataset(args.dataset)
    X_train, X_test = interpolate_scale(X_train, X_test)

    model.fit(X_train, y_train)

    # TODO Do the same for train data
    edata = model.embeddings(X_test).with_columns([pl.lit(args.dataset).alias('dataset'), pl.lit('test').alias('set')])
    edata.write_parquet(file_path)





