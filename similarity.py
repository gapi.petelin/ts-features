import polars as pl
import argparse
import numpy as np
from CKA import *
import seaborn as sns
from tqdm import tqdm
import itertools
import multiprocessing as mp
import itertools
import os

def pairwise_similarities(df1, df2):
    layers1 = sorted(df1['n_layer'].unique().to_list())
    layers2 = sorted(df2['n_layer'].unique().to_list())
    layer_pairs = list(itertools.product(layers1, layers2))

    df1 = df1.sort('n_example')
    df2 = df2.sort('n_example')
    
    sorted_embeddings1 = {
        i: np.array(df1.filter(pl.col('n_layer') == i)['embedding'].to_list()) 
        for i in layers1
    }
    sorted_embeddings2 = {
        j: np.array(df2.filter(pl.col('n_layer') == j)['embedding'].to_list()) 
        for j in layers2
    }
    
    cka = CKA()

    similarities = []
    for i, j in tqdm(layer_pairs, total=len(layer_pairs)):
        E1 = sorted_embeddings1[i]
        E2 = sorted_embeddings2[j]
        s = cka.kernel_CKA(E1, E2)
        similarities.append({'n_layer1': i, 'n_layer2': j, 'cka': s})
    return pl.DataFrame(similarities)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--method1', type=str, required=True)
    parser.add_argument('--method2', type=str, required=True)
    parser.add_argument('--seed1', type=int, required=True)
    parser.add_argument('--seed2', type=int, required=True)

    args = parser.parse_args()
    print(args)

    
    df1 = pl.read_parquet(f'embeddings/model_{args.method1}__dataset_{args.dataset}__seed_{args.seed1}__set_test.parquet')
    df2 = pl.read_parquet(f'embeddings/model_{args.method2}__dataset_{args.dataset}__seed_{args.seed2}__set_test.parquet')

    s = pairwise_similarities(df1, df2)

    folder_path = 'similarities'
    os.makedirs(folder_path, exist_ok=True)
    file_name = '__'.join([f'{key}_{value}' for key, value in sorted(vars(args).items())])
    s.write_parquet(f'{folder_path}/{file_name}.parquet')
