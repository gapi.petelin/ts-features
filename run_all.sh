#!/bin/bash

tsp -S 1
for SEED in 1 0
do
    for DATASET in "Car" "Coffee" "Adiac" "Trace" "Rock" "PowerCons" "InsectWingbeatSound"
    do
        for METHOD in "cnn" "inception" "tapnet"
        do
            tsp python representations.py --seed $SEED --dataset $DATASET --method $METHOD
        done
    done
done

#tsp -S 10
for SEED in 1 0
do
    for DATASET in "Car" "Coffee" "Adiac" "Trace" "HandOutlines" "Rock" "PowerCons" "InsectWingbeatSound"
    do
        for METHOD1 in "cnn" "inception" "tapnet"
        do
            for METHOD2 in "cnn" "inception" "tapnet"
            do
                tsp python similarity.py --dataset $DATASET --method1 $METHOD1 --method2 $METHOD2 --seed1 $SEED --seed2 $SEED
            done
        done
    done
done


